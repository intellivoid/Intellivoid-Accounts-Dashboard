<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png?v=39djAdAm3a">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png?v=39djAdAm3a">
<link rel="icon" type="image/png" sizes="194x194" href="/assets/favicon/favicon-194x194.png?v=39djAdAm3a">
<link rel="icon" type="image/png" sizes="192x192" href="/assets/favicon/android-chrome-192x192.png?v=39djAdAm3a">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png?v=39djAdAm3a">
<link rel="manifest" href="/assets/favicon/site.webmanifest?v=39djAdAm3a">
<link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg?v=39djAdAm3a" color="#0404fc">
<link rel="shortcut icon" href="/assets/favicon/favicon.ico?v=39djAdAm3a">
<meta name="apple-mobile-web-app-title" content="Intellivoid Accounts">
<meta name="application-name" content="Intellivoid Accounts">
<meta name="msapplication-TileColor" content="#0404fc">
<meta name="msapplication-TileImage" content="/assets/favicon/mstile-144x144.png?v=39djAdAm3a">
<meta name="msapplication-config" content="/assets/favicon/browserconfig.xml?v=39djAdAm3a">
<meta name="theme-color" content="#ffffff">
<meta name="title" content="Intellivoid Accounts">
<meta name="description" content="Secured second-generation authentication solution, a one-account for all by Intellivoid Technologies">
<meta property="og:type" content="website">
<meta property="og:url" content="https://accounts.intellivoid.net/">
<meta property="og:title" content="Intellivoid Accounts">
<meta property="og:description" content="Secured second-generation authentication solution, a one-account for all by Intellivoid Technologies">
<meta property="og:image" content="/assets/images/preview.png">
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://accounts.intellivoid.net/">
<meta property="twitter:title" content="Intellivoid Accounts">
<meta property="twitter:description" content="Secured second-generation authentication solution, a one-account for all by Intellivoid Technologies">
<meta property="twitter:image" content="/assets/images/preview.png">
<link rel="stylesheet" href="/assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="/assets/vendors/iconfonts/puse-icons-feather/feather.css">
<link rel="stylesheet" href="/assets/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="/assets/vendors/css/vendor.bundle.addons.css">
<link rel="stylesheet" href="/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" href="/assets/css/shared/style.css">
<link rel="stylesheet" href="/assets/css/iva_main/style.css">
<link rel="stylesheet" href="/assets/css/extra.css">
<link rel="stylesheet" href="/assets/css/animate.css">