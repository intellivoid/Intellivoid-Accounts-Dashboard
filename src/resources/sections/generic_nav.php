<?php
    use DynamicalWeb\DynamicalWeb;
    use DynamicalWeb\HTML;

?>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <a href="<?PHP DynamicalWeb::getRoute('index', array(), true); ?>">
                        <img src="/assets/images/logo_2.svg" alt="Intellivoid Accounts Brand" style="width: 130px; height: 30px;" class="img-fluid my-2">
                    </a>

                </div>
                <ul class="nav navbar-nav float-right mr-2">
                    <li class="nav-item d-block">
                        <a class="nav-link" href="<?PHP DynamicalWeb::getRoute('index', array(), true); ?>">
                            <i class="ficon feather icon-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>