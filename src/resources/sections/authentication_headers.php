<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="application-name" content="Intellivoid Accounts">
<meta name="msapplication-TileColor" content="#0404fc">
<meta name="msapplication-TileImage" content="/assets/favicon/mstile-144x144.png?v=39djAdAm3a">
<meta name="msapplication-config" content="/assets/favicon/browserconfig.xml?v=39djAdAm3a">
<meta name="theme-color" content="#3a50dc">
<meta name="title" content="Intellivoid Accounts">
<meta name="description" content="Secured second-generation authentication solution, a one-account for all by Intellivoid Technologies">
<meta property="og:type" content="website">
<meta property="og:url" content="https://accounts.intellivoid.net/">
<meta property="og:title" content="Intellivoid Accounts">
<meta property="og:description" content="Secured second-generation authentication solution, a one-account for all by Intellivoid Technologies">
<meta property="og:image" content="/assets/images/preview.png">
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://accounts.intellivoid.net/">
<meta property="twitter:title" content="Intellivoid Accounts">
<meta property="twitter:description" content="Secured second-generation authentication solution, a one-account for all by Intellivoid Technologies">
<meta property="twitter:image" content="/assets/images/preview.png">
<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png?v=39djAdAm3a">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png?v=39djAdAm3a">
<link rel="icon" type="image/png" sizes="194x194" href="/assets/favicon/favicon-194x194.png?v=39djAdAm3a">
<link rel="icon" type="image/png" sizes="192x192" href="/assets/favicon/android-chrome-192x192.png?v=39djAdAm3a">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png?v=39djAdAm3a">
<link rel="manifest" href="/assets/favicon/site.webmanifest?v=39djAdAm3a">
<link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg?v=39djAdAm3a" color="#0404fc">
<link rel="shortcut icon" href="/assets/favicon/favicon.ico?v=39djAdAm3a">
<meta name="apple-mobile-web-app-title" content="Intellivoid Accounts">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
<link rel="stylesheet" href="/assets/css/animate.css">
<link rel="stylesheet" type="text/css" href="/assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-extended.css">
<link rel="stylesheet" type="text/css" href="/assets/css/colors.css">
<link rel="stylesheet" type="text/css" href="/assets/css/extra.css">
<link rel="stylesheet" type="text/css" href="/assets/css/components.css">
<link rel="stylesheet" type="text/css" href="/assets/css/themes/dark-layout.css">
<link rel="stylesheet" type="text/css" href="/assets/css/themes/semi-dark-layout.css">
<link rel="stylesheet" type="text/css" href="/assets/css/core/menu/menu-types/horizontal-menu.css">
<link rel="stylesheet" type="text/css" href="/assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="/assets/css/pages/authentication.css">