### Intellivoid
When you use our services, you are trusting us with your information. You choose what information you want to provide; we will never obligate you to provide unnecessary information we don't require from you. We understand the responsibility we hold when it comes to something like protecting your data. We want you to be in control, this privacy policy will explain in detail on what data we collect from you and how we use it and how it's stored.


### Creating an Account
You can use Intellivoid Accounts to login to other Intellivoid Services when needed to. When you create an account, we require

 - A Username
 - An Email Address
 - A Password

Your username can be whatever you want it to be, it must be a unique in which it identifies your Intellivoid Account. This does not need to include your personal name, address or any identifying features about you. You can set your username to be whatever you want it to be for as long as Intellivoid Accounts considers it to be a valid username. You cannot change this username, so choose wisely. This is considered public by default.

Your Email Address is used to prove your ownership of the account or to allow Intellivoid to reach out to you for the purpose of providing technical support or to advise you of any updates to Intellivoid Accounts. We will never make your Email Address publicly available, send unwanted emails or promotions or share your email with anyone or anything outside of Intellivoid. Rarely you will never get any emails from Intellivoid by automated means, not even for password resets. You can change your Email via the Intellivoid Accounts Dashboard at anytime for as long as it is not used by another account. But you cannot leave your account without an email.

Your Password must be unique, and you should not reuse the same password you've used elsewhere. We take this password and hash it and store it on our servers. You can update your password via Intellivoid Accounts Dashboard at any time, your password is not sent to external sources and it is processed and validated all on our servers. We cannot know or see your password because of how the it is stored.

You can choose to provide information such as your legal First Name, Last Name and Birthday information. This information is optional and we will never obligate you to provide this information, like your email this information is also private and can be used to verify that you are the owner of the account in the case you lose access to your account. Intellivoid will not use this information for any other purpose.


### What information we collect

The information we collect includes unique identifiers, browser type and settings, device type and settings, operating system and application version number. We also collect information about the interaction of browsers and devices with our services, including IP address, and the date, time, and referrer URL of your request.

We collect this information to provide you the services we have available and to ensure that our services are working as intended such as tracking outages or troubleshooting technical issues which cannot be resolved from the client side and to make improvements to our services.

We do not collect information about you to show you ads, targeted promotions or to even share, sell or profit off your information. We do not show ads. We may show sponsorships time to time, but these sponsorships are provided as is and does not use your information to show "relative sponsorships". Your information is yours and we use non-personal, non-identifiable information to improve our services.

We also use data to determine how our services are used, we analyze this data to optimize performance issues and or to provide improvements.

We use this information to protect our service from attacks, government-backed attacks and security threats. This is used to detect issues, prevent and respond to issues accordingly. We may alert you if we notice security issues within our services via Email or via our services.

You can always request us via Intellivoid Support to send you the information we have on you and or outright delete your information.


### Managing, reviewing, and updating your information

When you are signed in you can always review and update your information by visiting Intellivoid Accounts, this is designed to help you to setup extra security on your account and to review the changes made to your account.


### Sharing your information / authorizing to third-party services

Intellivoid Accounts is designed to allow you to login to other third-party services, applications and websites if they integrated Intellivoid Accounts, these services can request special permissions from your account such as your personal details, your email and username and avatar.

By default, your username and avatar are public, this cannot be opted out because it allows the service to identify your Intellivoid Account by your username and display the respective avatar if designed to do so.

Only official Intellivoid Services are authorized to process purchases you make; this is a permission you cannot opt out on because it is required for services that requires this permission to function correctly. You will always be prompted before you make a purchase or start a subscription followed by a password confirmation to verify that you are making the purchase.

The reset of the permissions, you can choose to allow or not, which can consist of

 - Access to read Personal Information
 - Access to retrieve your Email Address
 - Access to send you notifications via Telegram


Third-party services can request access to read your personal information that you have set in your Intellivoid Account, such as your First Name, Last Name and Birthday. If you do not have this information set in your account, then the service will not get this information but can access this information as soon as you set such information. You are not obligated to allow this permission or fill in the required information.

Third-party services can request access to retrieve your Email Address, but they cannot edit it.

Third-party services can request the permission to send you notifications via Telegram if you have linked your Telegram account to your Intellivoid Account, this will not provide information about your Telegram Account or any unique identifier about your Telegram Account, this does not grant the permission to get information about your Telegram Account, they can only request Intellivoid Accounts to send the notification if you have a telegram account linked, if not; Nothing will be sent.

When you give permissions or access to third party services, Intellivoid is not responsible for how they handle or store your information, these services may have their own Privacy Policy and Terms of Service applicable to how they handle user data. You should do your research before trusting a third-party service with your data. However, in some cases you may want to authorize to official Intellivoid services using this method of authentication, the privacy policy and or terms of service can be found on the respective website or application of the service.


### Legal reasons

We will share personal information outside of Intellivoid if we have a good-faith belief that access, use, preservation, or disclosure of the information is reasonably necessary to:

 - Meet any applicable law, regulation, legal process, or enforceable governmental request.
 - Enforce applicable Terms of Service, including investigation of potential violations.
 - Detect, prevent, or otherwise address fraud, security, or technical issues.
 - Protect against harm to the rights, property or safety of Intellivoid, our users, or the public as required or permitted by law.

we'll continue to ensure the confidentiality of your personal information and give affected users notice before personal information is transferred or becomes subject to a different privacy policy.


### Security

Intellivoid Accounts is built with strong security features that continuously protect your information. The insights we gain from maintaining our services help us detect and automatically block security threats from ever reaching you. And if we do detect something risky that we think you should know about; we'll notify you and help guide you through steps to stay better protected.

We work hard to protect you and Intellivoid from unauthorized access, alteration, disclosure, or destruction of information we hold, including:

 - We use encryption to keep your data private while in transit
 - We offer two factor authentications to prevent unauthorized access to your account
 - We review our information collection, storage, and processing practices, including physical security measures, to prevent unauthorized access to our systems
 - We restrict access to personal information to Intellivoid employees, contractors, and agents who need that information in order to process it. Anyone with this access is subject to strict contractual confidentiality obligations and may be disciplined or terminated if they fail to meet these obligations.

Intellivoid also uses the haveibeenpwnd service to determine if your password has been compromised by another service, in order to determine this Intellivoid will hash your password using SHA1 and send the first 5 characters of the hash, Intellivoid will never send your full password to third-party services. This information is used to show you security alerts about your password being compromised to urge you to change your password.

### Data Transfers

We maintain servers around the world and your information may be processed on servers located outside of the country where you live. Data protection laws vary among countries, with some providing more protection than others. Regardless of where your information is processed, we apply the same protections described in this policy.

When we receive formal written complaints, we respond by contacting the person who made the complaint. We work with the appropriate regulatory authorities, including local data protection authorities, to resolve any complaints regarding the transfer of your data that we cannot resolve with you directly.


### When this policy applies

This Privacy Policy applies to all the services provided by Intellivoid including Intellivoid Accounts. This Privacy Policy doesn't apply to services that have separate privacy policies that do not incorporate this Privacy Policy. This Privacy Policy doesn't apply to Services offered by other companies or individuals, including products or sites that may include Intellivoid services or be linked from our services