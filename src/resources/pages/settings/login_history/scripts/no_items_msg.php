<?php
    use DynamicalWeb\HTML;
?>
<div class="container text-center pt-3">
    <h4 class="mb-1 mt-2 text-muted"><?PHP HTML::print(TEXT_NO_RESULTS_HEADER); ?></h4>
    <p class="w-75 mx-auto mb-5 text-muted"><?PHP HTML::print(TEXT_NO_RESULTS_TEXT); ?></p>
</div>