<?php
    use DynamicalWeb\HTML;
?>

<div class="table-responsive mt-0">
    <table class="table table-hover-animation mb-0">
        <thead>
            <tr>
                <th><?PHP HTML::print(TEXT_TABLE_HEADERS_ORIGIN); ?></th>
                <th><?PHP HTML::print(TEXT_TABLE_HEADERS_STATUS); ?></th>
                <th><?PHP HTML::print(TEXT_TABLE_HEADERS_BROWSER); ?></th>
                <th><?PHP HTML::print(TEXT_TABLE_HEADERS_IP_ADDRESS); ?></th>
                <th><?PHP HTML::print(TEXT_TABLE_HEADERS_DATE); ?></th>
            </tr>
        </thead>
        <tbody>
            <?PHP HTML::importScript('render_table_items'); ?>
        </tbody>
    </table>
</div>

