<?PHP
    use DynamicalWeb\HTML;
?>
<div class="d-flex flex-column justify-content-center align-items-center" style="height:30vh;">
    <div class="my-flex-item">
        <h3 class="text-muted"><?PHP HTML::print(TEXT_NO_ITEMS); ?></h3>
    </div>
</div>